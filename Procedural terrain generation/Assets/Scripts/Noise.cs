﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Noise
{
    #region properties
    public enum NormalizeMode { Local, Global }
    #endregion

    #region method

    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, NoiseSettings noiseSettings, Vector2 sampleCenter)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        float firstLayerValue = 1;
        float maxPossibleHeight = 0;
        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;
        System.Random prng = new System.Random(noiseSettings.seed);
        foreach (NoiseSettingLayer noiseSettingLayer in noiseSettings.noiseSettingLayers)
        {
            bool isFirstLayer = noiseSettings.noiseSettingLayers.IndexOf(noiseSettingLayer) == 0;

            Vector2[] octaveOffsets = new Vector2[noiseSettingLayer.octaves];

            float amplitude = 1;
            for (int i = 0; i < noiseSettingLayer.octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + noiseSettingLayer.offset.x + sampleCenter.x;
                float offsetY = prng.Next(-100000, 100000) - noiseSettingLayer.offset.y - sampleCenter.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);
                maxPossibleHeight += amplitude;
                amplitude *= noiseSettingLayer.persistance;
            }

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1;
                    float frequency = 1;
                    float noiseHeight = 0;

                    for (int i = 0; i < noiseSettingLayer.octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / noiseSettingLayer.scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / noiseSettingLayer.scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= noiseSettingLayer.persistance;
                        frequency *= noiseSettingLayer.lacunarity;
                    }

                    if (noiseHeight > maxLocalNoiseHeight)
                        maxLocalNoiseHeight = noiseHeight;
                    if (noiseHeight < minLocalNoiseHeight)
                        minLocalNoiseHeight = noiseHeight;

                    if (isFirstLayer)
                        firstLayerValue = noiseHeight;

                    noiseMap[x, y] += noiseHeight;
                    if (noiseSettings.normalizeMode == NormalizeMode.Global)
                    {
                        float normalizedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight / 0.9f);
                        noiseMap[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                    }
                }
            }
        }

        if (noiseSettings.normalizeMode == NormalizeMode.Local)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                }
            }
        }
        return noiseMap;
    }
    #endregion
}

[System.Serializable]
public class NoiseSettings
{
    #region properties
    public Noise.NormalizeMode normalizeMode = Noise.NormalizeMode.Global;
    public int seed;
    public List<NoiseSettingLayer> noiseSettingLayers;
    #endregion
}
[System.Serializable]
public class NoiseSettingLayer
{
    #region properties
    [Min(0.01f)]
    public float scale = 50;
    [Range(1, 18)]
    public int octaves = 6;
    [Range(0, 1)]
    public float persistance = .6f;
    [Min(1)]
    public float lacunarity = 2;
    public Vector2 offset;
    #endregion

    #region method
    /// <summary>
    /// be sure that value are in valid range
    /// </summary>
    public void ValidateValues()
    {
        scale = Mathf.Max(scale, 0.01f);
        octaves = Mathf.Max(octaves, 1);
        lacunarity = Mathf.Max(lacunarity, 1);
        persistance = Mathf.Clamp01(persistance);
    }
    #endregion
}
