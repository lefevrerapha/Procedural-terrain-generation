using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChunk
{
    #region properties
    public event System.Action<TerrainChunk, bool> OnVisibilityChanged;
    const float colliderGenerationDistanceTreshHold = 5f;
    public Vector2 coord;

    public GameObject meshObject;
    Vector2 sampleCenter;
    Bounds bounds;

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    MeshCollider meshCollider;

    List<LODInfo> detailLevels;
    List<LODMesh> lodMeshs = new List<LODMesh>();
    int colliderLODIndex;

    HeightMap heightMap;
    bool heightMapReceived;
    int previousLodIndex = -1;
    bool hasSetCollider;
    float maxViewDst;

    HeightMapSettings heightMapSettings;
    MeshSettings meshSettings;
    Transform viewer;
    private static int index = 0;
    Vector2 viewerPosition
    {
        get
        {
            return new Vector2(viewer.position.x, viewer.position.z);
        }
    }

    #endregion

    #region constructor
    public TerrainChunk(Vector2 coord, HeightMapSettings heightMapSettings, MeshSettings meshSettings, List<LODInfo> detailLevels, int colliderLODIndex, Transform parent, Transform viewer, Material material)
    {
        this.coord = coord;
        this.colliderLODIndex = colliderLODIndex;
        this.detailLevels = detailLevels;
        this.heightMapSettings = heightMapSettings;
        this.meshSettings = meshSettings;
        this.viewer = viewer;

        sampleCenter = coord * meshSettings.meshWorldSize / meshSettings.scale;
        Vector2 position = coord * meshSettings.meshWorldSize;
        bounds = new Bounds(position, Vector2.one * meshSettings.meshWorldSize);
        index = index + 1;
        meshObject = new GameObject("Terrain Chunk " + index);
        meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshFilter = meshObject.AddComponent<MeshFilter>();
        meshCollider = meshObject.AddComponent<MeshCollider>();
        meshRenderer.material = material;

        meshObject.transform.position = new Vector3(position.x, 0, position.y);
        meshObject.transform.parent = parent;
        SetVisible(false);

        foreach (LODInfo detailLevel in detailLevels)
        {
            LODMesh lODMesh = new LODMesh(detailLevel.lod);
            lodMeshs.Add(lODMesh);
            lODMesh.updateCallback += UpdateTerrainChunk;
            if (lodMeshs.IndexOf(lODMesh) <= colliderLODIndex)
                lODMesh.updateCallback += UpdateCollisionMesh;
        }
        maxViewDst = detailLevels[detailLevels.Count - 1].visibleDstThreshold;
    }
    #endregion

    #region method
    /// <summary>
    /// to be sure that 1 create the terrainChunk, 2 put the OnVisibilityChange, 3 request the data(it can be long)
    /// </summary>
    public void Load()
    {
        ThreadedDataRequester.RequestData(() => HeightMapGenerator.GenerateHeightMap(meshSettings.numVertsPerline, meshSettings.numVertsPerline, heightMapSettings, sampleCenter), OnHeightMapReceived);

    }
    void OnHeightMapReceived(object heightMapObject)
    {
        this.heightMap = (HeightMap)heightMapObject;
        heightMapReceived = true;

        UpdateTerrainChunk();
    }

    public void UpdateTerrainChunk()
    {
        if (heightMapReceived)
        {
            float viewerDstFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(viewerPosition));
            bool wasVisible = IsVisible();
            bool visible = viewerDstFromNearestEdge <= maxViewDst;
            if (visible)
            {
                int lodIndex = 0;
                for (int i = 0; i < detailLevels.Count - 1; i++)
                {
                    if (viewerDstFromNearestEdge > detailLevels[i].visibleDstThreshold)
                        lodIndex = i + 1;
                    else
                        break;
                }
                if (lodIndex != previousLodIndex)
                {
                    LODMesh lODMesh = lodMeshs[lodIndex];
                    if (lODMesh.hasMesh)
                    {
                        previousLodIndex = lodIndex;
                        meshFilter.mesh = lODMesh.mesh;
                    }
                    else if (!lODMesh.hasRequestedMesh)
                        lODMesh.RequestMesh(heightMap, meshSettings);
                }
            }

            if (wasVisible != visible)
            {
                SetVisible(visible);
                OnVisibilityChanged?.Invoke(this, visible);
                if (!visible)
                    UnityEngine.Object.Destroy(this.meshObject);
            }

        }
    }

    public void UpdateCollisionMesh()
    {
        if (!hasSetCollider)
        {
            float sqrDstFromViewerToEdge = bounds.SqrDistance(viewerPosition);
            if (sqrDstFromViewerToEdge < detailLevels[colliderLODIndex].sqrVisibleDstThreshold)
                if (!lodMeshs[colliderLODIndex].hasRequestedMesh)
                    lodMeshs[colliderLODIndex].RequestMesh(heightMap, meshSettings);

            if (sqrDstFromViewerToEdge < colliderGenerationDistanceTreshHold * colliderGenerationDistanceTreshHold)
            {
                if (lodMeshs[colliderLODIndex].hasMesh)
                {
                    meshCollider.sharedMesh = lodMeshs[colliderLODIndex].mesh;
                    hasSetCollider = true;
                }
            }
        }
    }

    public void SetVisible(bool visible)
    {
        meshObject.SetActive(visible);
    }

    public bool IsVisible()
    {
        return meshObject.activeSelf;
    }
    #endregion
    /// <summary>
    /// level of detail
    /// </summary>
    class LODMesh
    {
        #region properties
        public Mesh mesh;
        public bool hasRequestedMesh;
        public bool hasMesh;
        int lod;
        public event Action updateCallback;
        #endregion

        #region constructor
        public LODMesh(int lod)
        {
            this.lod = lod;
        }
        #endregion

        #region method
        void OnMeshDataReceived(object meshDataObject)
        {
            mesh = ((MeshData)meshDataObject).CreateMesh();
            hasMesh = true;
            updateCallback();
        }

        public void RequestMesh(HeightMap heightMap, MeshSettings meshSettings)
        {
            hasRequestedMesh = true;
            ThreadedDataRequester.RequestData(() => MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, lod), OnMeshDataReceived);
        }
        #endregion
    }
}
