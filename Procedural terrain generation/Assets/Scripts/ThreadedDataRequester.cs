using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ThreadedDataRequester : MonoBehaviour
{
    #region properties
    Queue<ThreadingInfo> dataQueue = new Queue<ThreadingInfo>();
    static ThreadedDataRequester instance;
    #endregion

    #region method
    void Awake()
    {
        instance = FindObjectOfType<ThreadedDataRequester>();
    }
    public static void RequestData(Func<object> generateData, Action<object> callback)
    {
        ThreadStart threadStart = delegate
        {
            instance.DataThread(generateData, callback);
        };
        new Thread(threadStart).Start();
    }

    void DataThread(Func<object> generateData, Action<object> callback)
    {
        object data = generateData();
        lock (dataQueue)
        {
            dataQueue.Enqueue(new ThreadingInfo(callback, data));
        }
    }


    void Update()
    {
        lock (dataQueue)
        {
            while (dataQueue.Count > 0)
            {
                ThreadingInfo threadInfo = dataQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    #endregion

    struct ThreadingInfo
    {
        public readonly Action<object> callback;
        public readonly object parameter;

        public ThreadingInfo(Action<object> callback, object parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }
}
