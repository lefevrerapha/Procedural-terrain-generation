using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class HeightMapSettings : UpdatableData
{
    #region properties
    public NoiseSettings noiseSettings;

    public bool useFalloffMap;
    public float heightMultiplier;
    public AnimationCurve heightCurve;
    public float minHeight
    {
        get
        {
            return heightMultiplier * heightCurve.Evaluate(0);
        }
    }

    public float maxHeight
    {
        get
        {
            return heightMultiplier * heightCurve.Evaluate(1);
        }
    }
    #endregion

    #region method
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        foreach (NoiseSettingLayer noiseSettingLayer  in noiseSettings.noiseSettingLayers)
            noiseSettingLayer.ValidateValues();

        base.OnValidate();
    }
#endif
    #endregion
}
