using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UpdatableData : ScriptableObject
{
    public event System.Action OnvaluesUpdated;
    public bool autoUpdate;
#if UNITY_EDITOR
    protected virtual void OnValidate()
    {
        if (autoUpdate)
            UnityEditor.EditorApplication.update += NotififyOfUpdatedValues;
    }

    public void NotififyOfUpdatedValues()
    {
        UnityEditor.EditorApplication.update -= NotififyOfUpdatedValues;
        if (OnvaluesUpdated != null)
            OnvaluesUpdated();
    }
#endif
}
