using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MeshSettings : UpdatableData
{
    #region properties
    public float scale = 2.5f;
    public bool useFlatShading;

    public const int numSupportedLODs = 5;
    public const int numberSupportedChunkSizes = 9;
    public const int numberSupportedFlatShadedChunckSizes = 3;
    public static readonly int[] supportedChunkSizes = { 48, 72, 96, 120, 144, 168, 192, 216, 240 };

    [Range(0, numberSupportedChunkSizes - 1)]
    public int chunckSizeIndex;
    [Range(0, numberSupportedFlatShadedChunckSizes - 1)]
    public int flatShadedChunckSizeIndex;

    /// <summary>
    /// can't be too heigh because there is a limit of number of vertice in 1 mesh
    /// mapChunkSize + 1 have to be divisable by (meshSimplificationIncrement) 1, 2 ,4, 6, 8
    /// have to be <96 for flat shading
    /// have to be <240 for else
    /// num verts per lion of mesh at LOD=0.includes the 2 extra vertices that are exlcude from final mesh , but used for calculted normals
    /// </summary>
    public int numVertsPerline
    {
        get
        {
            return supportedChunkSizes[useFlatShading ? flatShadedChunckSizeIndex : chunckSizeIndex] + 5;
        }
    }
    public float meshWorldSize
    {
        get
        {
            return (numVertsPerline - 3) * scale;
        }
    }
    #endregion
}
